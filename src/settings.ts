
export class Settings<T extends { [key: string]: any }> {

    readonly name: string;
    readonly props: T;

    private _onChange?: (props: Partial<T>) => void;

    constructor(props: T, name: string = "settings") {
        this.props = props;
        this.name = name;
    }

    getProps(): T {
        return this.props;
    }

    setProps(props: Partial<T>): this {
        Object.keys(props).forEach(p => ((this.props as any)[p] = props[p]));
        this._onChange && this._onChange(props);
        return this;
    }

    getProp<K extends keyof T>(key: K): T[K] {
        return (this.props)[key];
    }

    setProp<K extends keyof T>(key: K, value: T[K]): this {
        (this.props)[key] = value;
        const props: Partial<T> = {};
        props[key] = value;
        this._onChange && this._onChange(props);
        return this;
    }

    onChange(callback: (props: Partial<T>) => void): this {
        this._onChange = callback;
        return this;
    }

}

// Test: npx ts-node src/settins.ts

// const x = { n: 2, s: "s" };
// const s = new Settings<typeof x>(x);

// s.setProps({ n: 3 });

// console.log(typeof s.getProp("n")); // number
// console.log(typeof s.getProp("s")); // string

// s.setProps(JSON.parse(window.localStorage[s.name]));
// s.onChange(() => (window.localStorage[s.name] = JSON.stringify(s.props)));

// const settingsInit = JSON.parse(localStorage.getItem("settings") ?? "{}");
// const settings = new Settings<typeof settingsInit>(settingsInit);
// settings.onChange((props) => localStorage.setItem("settings", JSON.stringify(props)));
