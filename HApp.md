# HApp - HSML Application

`HApp` - HSML App, Javascript/Typescript Web UI framework for rapid SPA web applications development.

- `HSML` - hyper script markup language
- `SPA` - single page web app

## HApp prototyping

HApp can be used as Javascript included in HTML to avoid build process for simple cases and quick prototyping.
See live [demo](https://peryl.gitlab.io/peryl/demo/js/hsml-app-js_demo.html)
or [demo](https://peryl.gitlab.io/peryl/demo/js/hsml-app-js-happi_demo.html).

## HApp Concept

Concept is based on:

- `state` - Represent application state
- `view` - Template function rendering app UI as HSML markup. To convert HTML into HSML you can use [online converter](https://peryl.gitlab.io/peryl/dist/demo/hsml-convert_demo.html)
- `dispatcher` - Action dispatcher covering app logic, change state base on actions and request application update UI.

![Schema](HApp.svg)

Basic HApp skeleton with `actions`, `state` init function, `view` template function and actions `dispatcher` function:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PeRyL HSML App</title>
</head>
<body>
    <div id="app"></div>
    <script src="./dist/browser-umd/hsml-app.js"></script>
    <!-- <script src="https://unpkg.com/peryl/dist/browser-umd/hsml-app.js"></script> -->
    <script type="module">
        // import { HApp, HAppActions } from "./dist/browser-esmodule/hsml-app.js";
        // import { HApp, HAppActions } from "https://unpkg.com/peryl/dist/browser-esmodule/hsml-app.js";

        // Actions definition
        const Actions = {
            say: "say",
            fetch: "fetch"
        }

        // App state definition and initialization
        function state() {
            return {
                message: "",
                json: ""
            };
        }

        // Template function, returns HSML markup generated from app state
        function view(state) {
            return [
                ["p", [
                    "Greeting: ", state.message
                ]],
                ["p", [
                    // On button event "click" dispatch Actions.say type with data "Hello"
                    ["button", { on: ["click", Actions.say, "Hello"] }, "Say Hello"],
                    " ",
                    ["button", { on: ["click", Actions.say, "Hi"] }, "Say Hi"],
                ]],
                ["p", [
                    ["button", { on: ["click", Actions.fetch] }, "Server fetch time"],
                    ["pre", state.json]
                ]]
            ];
        }

        // Action dispatcher, app logic
        async function dispatcher(action, state, dispatch) {
            console.log(action);
            switch (action.type) {
                // Dispatch action "say"
                case Actions.say:
                    // Change app state message by action data (3. parameter of on click action)
                    state.message = action.data;
                    break;
                case Actions.fetch:
                    // Server async call
                    try {
                        const res = await fetch("http://date.jsontest.com");
                        const data = await res.json();
                        state.json = JSON.stringify(data, null, 4);
                    } catch (error) {
                        state.json = String(error);
                    }
                    break;
            }
        }

        // Run application
        new HApp(state, view, dispatcher, "app");

    </script>
</body>
</html>
```

## HApp with Typescript

```sh
npm i peryl
```
