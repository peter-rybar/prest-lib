import { HApp, HDispatcher, HFormData, HState, HView } from "../src/hsml-app";

enum Actions {
    // change = "change",
    submit = "submit"
}

interface Data {
    amount: number;
    iban: string;
    variable: string;
}

interface State {
    form: HFormData<Data>;
    qrstring?: string;
}

const state: HState<State> = function () {
    return {
        form: {
            data: {
                amount: 1,
                iban: "",
                variable: ""
            },
            validation: {}
        }
    };
};

const view: HView<State, Actions> = function (state) {
    return [
        ["div", [
            ["form",
                {
                    style: "margin-right: 10px;",
                    novalidate: true,
                    on: [
                        // ["change", Actions.change],
                        ["submit", Actions.submit]
                    ]
                },
                [
                    ["label",
                        { style: "display: block; margin-bottom: 5px;" },
                        [
                            "Amount:",
                            ["br"],
                            ["input", {
                                style: "width: 210px;",
                                type: "number",
                                name: "amount",
                                required: true,
                                min: 1,
                                max: 1000,
                                value: state.form.data.amount
                            }],
                            ["br"],
                            ["div", { style: "color:red;" }, state.form.validation.amount]
                        ]
                    ],
                    ["label",
                        { style: "display: block; margin-bottom: 5px;" },
                        [
                            "IBAN:",
                            ["br"],
                            ["input", {
                                style: "width: 210px;",
                                type: "text",
                                name: "iban",
                                minlength: 10,
                                required: true,
                                value: state.form.data.iban
                            }],
                            ["br"],
                            ["div", { style: "color:red;" }, state.form.validation.iban]
                        ]
                    ],
                    ["label",
                        { style: "display: block; margin-bottom: 5px;" },
                        [
                            "Variable:",
                            ["br"],
                            ["input", {
                                style: "width: 210px;",
                                type: "text",
                                name: "variable",
                                value: state.form.data.variable
                            }],
                            ["br"],
                            ["div", { style: "color:red;" }, state.form.validation.variable]
                        ]
                    ],
                    ["button", "Generate"],
                    ["pre", state.qrstring],
                    state.qrstring
                        ? ["img", {
                            src: `https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L|1&chl=${encodeURIComponent(state.qrstring)}`,
                            style: "width:100%;max-width:500px",
                            alt: state.qrstring
                        }]
                        : null
                ]
            ]]
        ]
    ];
};

const dispatcher: HDispatcher<State, Actions> = async function (action, state, dispatch) {
    switch (action.type) {
        // case Actions.change:
        //     const input = action.data as HFormInputData;
        //     console.log(JSON.stringify(input, null, 4));
        //     state.form.data[input.name!] = input.value;
        //     state.form.validation[input.name!] = input.validation;
        //     break;
        case Actions.submit:
            const form = action.data as HFormData<Data>;
            console.log(JSON.stringify(form, null, 4));
            state.form = form;
            state.qrstring = undefined;
            if (form.valid) {
                state.qrstring = JSON.stringify(form.data)
            }
            break;

    }
};

(window as any).app = new HApp<State, Actions>(state, view, dispatcher, "app", true);
