import { HAttrOnData, HAttrOnDataFnc, HElement, HElements, HHandlerCtx, THSP } from "./hsml";
import { hsmls2idomPatch } from "./hsml-idom";

export type HState<State> = () => State;

export type HView<State, HActionType extends string> = (
    state: State
) => HElements<HActionType>;

export type HView1<State, HActionType extends string> = (
    state: State
) => HElement<HActionType>;

export type HAppAction =
    | "happ-init"
    | "happ-mount"
    | "happ-umount"
    | "happ-action"
    | "happ-attribute";

export enum HAppActions {
    init = "happ-init",
    mount = "happ-mount",
    umount = "happ-umount",
    action = "happ-action",
    attribute = "happ-attribute"
}

export interface HAction<HActionType extends string> {
    type: HActionType;
    data?: any;
    event?: Event;
}

export type HDispatchScope =
    | "element"
    | "window";

export enum HDispatchScopes {
    element = "element",
    window = "window"
}

export type HDispatch<HActionType extends string> = (
    type: HAction<HActionType>["type"],
    data?: HAction<HActionType>["data"],
    scope?: HDispatchScope | HDispatchScopes
) => Promise<void>;

type SkipUpdate = true;

export type HDispatcher<State, HActionType extends string> = (
    // this: HApp<State, HActionType>,
    action: HAction<HActionType | HAppAction | HAppActions>,
    state: State,
    dispatch: HDispatch<HActionType>
) => Promise<void | SkipUpdate>;

const schedule = window.requestAnimationFrame ||
    // window.webkitRequestAnimationFrame ||
    // (window as any).mozRequestAnimationFrame ||
    // (window as any).oRequestAnimationFrame ||
    // (window as any).msRequestAnimationFrame ||
    function (callback: Function) { window.setTimeout(callback, 1000 / 60); };

const unschedule = window.cancelAnimationFrame ||
    // window.webkitCancelAnimationFrame ||
    // (window as any).mozCancelAnimationFrame ||
    // (window as any).oCancelAnimationFrame ||
    // (window as any).msCancelAnimationFrame ||
    function (handle: number) { window.clearTimeout(handle); };

const msgAction = "action:";
const msgDispatch = "dispatch:";
const msgRender = "render:";
const msgUpdate = "update:";

const HAPP = "happ";

export interface HAppI<State, HActionType extends string> {
    state: HState<State>;
    view: HView<State, HActionType>;
    dispatcher: HDispatcher<State, HActionType>;
    element?: Element | string | null;
    debug?: boolean;
    /** Pattern ^[a-z][a-z0-9-]{0,30}[a-z0-9]$ */
    id?: string;
    attributes?: string[];
}

/**
 * HApp definition
 *
 * @param hAppI HApp definition
 * @returns HApp instance
 */
export function happ<State, HActionType extends string>(hAppI: {
    state: HState<State>;
    view: HView<State, HActionType>;
    dispatcher: HDispatcher<State, HActionType>;
    element: Element | string | null;
    debug?: boolean;
    /** Pattern ^[a-z][a-z0-9-]{0,30}[a-z0-9]$ */
    id?: string;
    // attributes?: string[];
}) {
    return new HApp<State, HActionType>(
        hAppI.state,
        hAppI.view,
        hAppI.dispatcher,
        hAppI.element,
        hAppI.debug,
        hAppI.id
        // hAppI!.attributes
    );
}

// export type Class<T = object> = new (...args: any[]) => T;

// export function happi<State, HActionType extends string>(
//     hAppI: Class<HAppI<State, HActionType>>,
//     element?: Element | string | null,
//     debug? boolean,
//     /** Pattern ^[a-z][a-z0-9-]{0,30}[a-z0-9]$ */
//     id?: string
//     // attributes?: string[]
// ) {
//     const hapi = new hAppI();
//     return new HApp<State, HActionType>(
//         hapi.state,
//         hapi.view,
//         hapi.dispatcher,
//         element,
//         debug
//     );
// }

// HAppEl

/**
 * HApp custom HTML element definition.
 *
 * @param hAppI HApp definition
 */
export function happel<State, HActionType extends string>(hAppI: {
    state: HState<State>;
    view: HView<State, HActionType>;
    dispatcher: HDispatcher<State, HActionType>;
    /** Pattern ^[a-z]{0,10}-[a-z0-9-]{0,30}[a-z0-9]$ */
    id: string;
    /** Element attribute list */
    attributes?: string[];
    debug?: boolean;
}): void {
    // condition to prevent rerunning on hot module reloads
    if (!customElements.get(hAppI.id)) {
        customElements.define(
            hAppI.id,
            class HAppElement extends HTMLElement {
                static get observedAttributes() {
                    const state = hAppI.state();
                    return hAppI.attributes ?? typeof state === "object"
                        ? Object.keys(state as object)
                        : [];
                }

                private _happel: HApp<State, HActionType>;

                constructor() {
                    super();
                    this._happel = new HApp<State, HActionType>(
                        hAppI.state,
                        hAppI.view,
                        hAppI.dispatcher,
                        undefined,
                        hAppI.debug,
                        hAppI.id
                        // hAppI.attributes
                    );
                    (this._happel as any).customElement = this;
                }

                connectedCallback() {
                    // this._happel.mount(this);
                    this.attachShadow({ mode: "open" });
                    this._happel.mount(this.shadowRoot as any);
                }

                disconnectedCallback() {
                    this._happel.umount();
                }

                adoptedCallback() {
                    this._happel.update();
                }

                attributeChangedCallback(
                    attrName: string,
                    oldVal: string | null,
                    newVal: string | null
                ) {
                    this._happel.dispatch(
                        HAppActions.attribute as any,
                        { attrName, oldVal, newVal });
                }
            }
        );
    }
}

/**
 * HSML App
 */
export class HApp<State, HActionType extends string> implements HHandlerCtx<HActionType> {

    static log = console.log;
    static error = console.error;
    static warn = console.warn;

    readonly state: State;
    readonly view: HView<State, HActionType>;
    readonly dispatcher: HDispatcher<State, HActionType>;

    debug: boolean;

    /** Pattern ^[a-z][a-z0-9-]{0,30}[a-z0-9]$ */
    readonly id: string;
    // readonly attributes?: string[];

    readonly element?: HTMLElement;
    readonly refs: { [key: string]: HTMLElement } = {};

    readonly customElement?: HTMLElement; // happ custom html element

    private _updateSched?: number;

    // private _onDispatch?: HDispatcher<State>;

    private _windowActionListener?: (event: Event) => void;

    /**
     * @param state State init function
     * @param view View renderer function
     * @param dispatcher Dispatcher function
     * @param element Root element
     * @param debug Debug mode
     * @param id HApp ID, pattern "^[a-z][a-z0-9-]{0,30}[a-z0-9]$"
     */
    constructor(
        state: HState<State>,
        view: HView<State, HActionType>,
        dispatcher?: HDispatcher<State, HActionType>,
        element?: Element | string | null,
        debug?: boolean ,
        id?: string,
        // attributes?: string[]
    ) {
        this.id = id ?? HAPP;
        this.debug = debug ?? false;
        // this.attributes = attributes ?? [];
        // this.attributes = attributes ?? typeof state() === "object"
        //     ? Object.keys(state() as object)
        //     : [];
        this.state = state();
        this.view = view;
        this.dispatcher = dispatcher ?? (async (a) => HApp.log(this.id, msgAction, a.type, a.data));
        this._dispatchAction(HAppActions.init, this).then(() => element && this.mount(element));
    }

    /**
     * Dispatch app action.
     */
    dispatch: HDispatch<HActionType> = async (
        type: HActionType,
        data?: any,
        scope?: HDispatchScope
    ): Promise<void> => {
        return this._dispatchAction(type, data, undefined, scope);
    }

    // onDispatch = (dispatcher: HDispatcher<State>): this => {
    //     this._onDispatch = dispatcher;
    //     return this;
    // }

    private async _dispatchAction(
        type: HActionType | HAppAction | HAppActions,
        data?: any,
        event?: Event,
        scope?: HDispatchScope
    ): Promise<void> {
        if (this.debug) {
            HApp.log(this.id, msgAction, { type, data, event });
            const t0 = performance.now();
            await this._dispatch(type, data, event, scope);
            const t1 = performance.now();
            HApp.log(this.id, msgDispatch, `${t1 - t0} ms`, this.state);
        } else {
            await this._dispatch(type, data, event, scope);
        }
    }

    private async _dispatch(
        type: HActionType | HAppAction | HAppActions,
        data: any,
        event?: Event,
        scope?: HDispatchScope
    ): Promise<void> {
        if (!scope) {
            try {
                const skipUpdate = await this.dispatcher(
                    { type: type as HActionType, data, event },
                    this.state,
                    this.dispatch
                );
                if (!skipUpdate) {
                    this.update();
                }
            } catch (e) {
                HApp.error(this.id, msgDispatch, e);
            }
        }
        scope === "element" && this._dispatchElement(type, data);
        scope === "window" && this._dispatchWindow(type, data);
    }

    private async _dispatchElement(
        type: HActionType | HAppAction | HAppActions,
        data?: any
    ): Promise<void> {
        this.customElement && elementDispatchCustomEvent(this.customElement, HAppActions.action, { type, data });
        !this.customElement && this.element && elementDispatchCustomEvent(this.element, HAppActions.action, { type, data });
        // this._onDispatch?.({ type, data, event }, this.state, this.dispatch);
    }

    private async _dispatchWindow(
        type: HActionType | HAppAction | HAppActions,
        data?: any): Promise<void> {
        window.dispatchEvent(new CustomEvent(HAppActions.action, { detail: { type, data } }));
    }

    /**
     * Window action listen and route actions to dispatcher.
     * Call this method on app mount action.
     * Listening will be stopped on app umount automatically.
     */
    windowActionListen() {
        if (!this._windowActionListener) {
            this._windowActionListener = (event: Event) => {
                const action = (event as CustomEvent).detail as HAction<HActionType>;
                this._dispatchAction(HAppActions.action, action);
            };
            window.addEventListener(HAppActions.action, this._windowActionListener);
        } else {
            HApp.warn("windowEventListener already added");
        }
    }

    /**
     * Render HSML based on app state.
     */
    render = (): HElements<HActionType> => {
        if (this.debug) {
            const t0 = performance.now();
            let hsmls;
            try {
                hsmls = this.view(this.state);
            } catch (e) {
                HApp.error(this.id, msgRender, e);
            }
            const t1 = performance.now();
            HApp.log(this.id, msgRender, `${t1 - t0} ms`, hsmls);
            return hsmls ?? [];
        } else {
            let hsmls;
            try {
                hsmls = this.view(this.state);
            } catch (e) {
                HApp.error(this.id, msgRender, e);
            }
            return hsmls ?? [];
        }
    }

    /**
     * HSML action callback.
     */
    actionCb = async (actionType: HActionType, data: HAttrOnData, event: Event): Promise<void> => {
        data = (data?.constructor === Function)
            ? (data as HAttrOnDataFnc)(event)
            : data;
        if (data === undefined && event) {
            if (event instanceof CustomEvent) {
                data = event.detail;
            } else {
                data = await formData(event) as any;
                // TODO middlewares for data processing
                // const middlewares: ((data: any, event: Event) => any)[] = [];
                // data = middlewares.reduce(
                //     (data, middleware) => middleware(data, event),
                //     data
                // );
            }
        }
        this._dispatchAction(actionType, data, event);
    }

    private _updateDom<HActionType extends string>(
        el: Element,
        hsml: HElements<HActionType>,
        ctx: HHandlerCtx<HActionType>
    ): void {
        if (this.debug) {
            const t0 = performance.now();
            hsmls2idomPatch(el, hsml, ctx);
            const t1 = performance.now();
            HApp.log(this.id, msgUpdate, `${t1 - t0} ms`, el);
        } else {
            hsmls2idomPatch(el, hsml, ctx);
        }
    }

    /**
     * Mount app to DOM element.
     *
     * @param e DOM element
     */
    mount = (e: Element | string | null): this => {
        const el = (typeof e === "string") ? document.getElementById(e) : e;
        if (el && (el as any)[HAPP]) {
            const a = (el as any)[HAPP] as HApp<State, HActionType>;
            a.umount();
        }
        if (el && !this.element) {
            (this as any).element = el;
            (el as any)[HAPP] = this;
            const hsmls = (this as any).render();
            this._updateDom<HActionType>(el, hsmls, this);
            this._dispatchAction(HAppActions.mount, this.element);
        }
        return this;
    }

    /**
     * Umount app to DOM element.
     */
    umount = (): this => {
        if (this.element) {
            this._dispatchAction(HAppActions.umount, this.element);
            if (this.element.hasAttribute(HAPP)) {
                this.element.removeAttribute(HAPP);
            }
            const aNodes = this.element.querySelectorAll(`[${HAPP}]`);
            for (let i = 0; i < aNodes.length; i++) {
                const a = (aNodes[i] as any).happ as HApp<State, HActionType>;
                a?.umount();
            }
            while (this.element.firstChild /*.hasChildNodes()*/) {
                this.element.removeChild(this.element.firstChild);
            }
            delete (this.element as any).happ;
            (this as any).element = undefined;
        }
        if (this._windowActionListener) {
            window.removeEventListener(HAppActions.action, this._windowActionListener);
        }
        return this;
    }

    /**
     * Update DOM element based on app state.
     */
    update = (): this => {
        if (this.element && !this._updateSched) {
            this._updateSched = schedule(() => {
                if (this.element) {
                    const hsmls = this.render();
                    this._updateDom<HActionType>(this.element, hsmls, this);
                }
                this._updateSched = undefined;
            });
        }
        return this;
    }

    toHsml = (): HElement<HActionType> => {
        if (this.element) {
            if (this._updateSched) {
                unschedule(this._updateSched);
                this._updateSched = undefined;
            } else {
                return ["div", { skip: true }];
            }
        }
        const hsmls = this.render();
        hsmls.push(
            (e: Element) => {
                if (!this.element) {
                    (this as any).element = e;
                    (e as any).happ = this;
                    this._dispatchAction(HAppActions.mount, this.element);
                }
            });
        return ["div", hsmls];
    }

    toHtml = (): string => {
        return this.element ? this.element.outerHTML : "";
    }

}

function elementDispatchCustomEvent<HActionType extends string>(
    el: HTMLElement,
    type: HActionType,
    data: any
) {
    el?.dispatchEvent(new CustomEvent(type, { detail: data }));
    (el as any)[`on${type}`]?.(new CustomEvent(type, { detail: data }));
}

type FormDataInputValue = string | number | boolean | null | Array<string | number | boolean | null>;

type FormInputData = {
    name?: string;
    value: FormDataInputValue;
    valueString?: string;
    valueNumber?: number | null;
    valueDate?: Date | null;
    valueFile?: File | FileList | null;
    validation: string;
    valid: boolean;
};

type FormData = {
    data: {
        [name: string]: FormDataInputValue;
    };
    validation: { [name: string]: string };
    valid: boolean;
};

export type HFormInputData<Value = FormDataInputValue> = {
    name?: string;
    value: Value;
    valueString?: string | null;
    valueNumber?: number | null;
    valueDate?: Date | null;
    valueFile?: File | FileList | null;
    validation: string;
    valid: boolean;
};

export type HFormData<Data> = {
    data: Data;
    validation: { [name in keyof Data]?: string };
    valid?: boolean;
};

async function formData(e: Event): Promise<FormData | FormInputData | undefined> {
    const el = e.target as HTMLElement;
    switch (el.nodeName) {
        case "FORM":
            (e as Event).preventDefault();
            const form: FormData = {
                data: {},
                validation: {},
                valid: true
            };
            const els = (el as HTMLFormElement).elements;
            for (let i = 0; i < els.length; i++) {
                const inputData = await formInputData(els[i]);
                if (inputData && inputData.name) {
                    const formData = form.data;
                    const name = inputData.name;
                    const value = inputData.value;
                    if (formData[name] === undefined) {
                        formData[name] = value;
                    } else if (typeof formData[name] === "string" || formData[name] instanceof String) {
                        if (value instanceof Array) {
                            formData[name] = [formData[name] as string, ...value];
                        } else {
                            formData[name] = [formData[name] as string, value as string];
                        }
                    } else if (formData[name] instanceof Array) {
                        if (value instanceof Array) {
                            formData[name] = (formData[name] as Array<any>).concat(value);
                        } else {
                            (formData[name] as Array<any>).push(value);
                        }
                    } else {
                        if (value instanceof Array) {
                            formData[name] = [formData[name] as string, ...value];
                        } else {
                            formData[name] = [formData[name] as string, value];
                        }
                    }
                    if (formData[name] instanceof Array) {
                        formData[name] = (formData[name] as Array<any>)
                            .filter(d => d !== null);
                        if ((els[i] as HTMLInputElement).type === "radio") {
                            formData[name] = (formData[name] as Array<any>).length
                                ? (formData[name] as Array<any>)[0]
                                : null;
                        }
                    }
                    if (inputData.validation) {
                        form.validation[name] = inputData.validation;
                    }
                    if (!inputData.valid) {
                        form.valid = false;
                    }
                }
            }
            return form;
        default:
            return await formInputData(el);
    }
}

async function formInputData(el: Element): Promise<FormInputData | undefined> {
    // Client-side form validation
    // https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation
    const vel = el as HTMLInputElement;
    if (vel.willValidate) {
        let valid = true;
        for (const key in vel.validity) {
            if (key !== "customError" && key !== "valid") {
                if ((vel.validity as any)[key]) {
                    const msgs = (vel as any)["validation"];
                    if (msgs) {
                        const msg = (msgs as any)[key];
                        if (msg) {
                            vel.setCustomValidity(msg);
                            // vel.reportValidity();
                            valid = false;
                            break;
                        } else {
                            vel.setCustomValidity("");
                            // vel.reportValidity();
                            break;
                        }
                    }
                }
            }
        }
        if (valid) {
            vel.setCustomValidity("");
            // vel.reportValidity();
        }
    }

    let data: FormInputData | undefined;
    switch (el.nodeName) {
        case "INPUT":
            const iel = el as HTMLInputElement;
            switch (iel.type) {
                case "text":
                case "hidden":
                case "password":
                case "email":
                case "search":
                case "url":
                case "tel":
                case "color":
                case "submit":
                case "button":
                    data = {
                        name: iel.name,
                        value: iel.value === "" ? null : iel.value,
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    break;
                case "number":
                case "range":
                    data = {
                        name: iel.name,
                        value: isNaN(iel.valueAsNumber)
                            ? null
                            : iel.valueAsNumber,
                        valueString: iel.value,
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    break;
                case "datetime-local":
                    data = {
                        name: iel.name,
                        value: iel.value === "" ? null : iel.value,
                        valueNumber: isNaN(iel.valueAsNumber)
                            ? null
                            : iel.valueAsNumber,
                        valueDate: isNaN(iel.valueAsNumber)
                            ? null
                            : new Date(iel.valueAsNumber),
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    break;
                case "date":
                case "month":
                case "time":
                case "week":
                    data = {
                        name: iel.name,
                        value: iel.value === "" ? null : iel.value,
                        valueNumber: isNaN(iel.valueAsNumber)
                            ? null
                            : iel.valueAsNumber,
                        valueDate: iel.valueAsDate,
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    break;
                case "radio":
                    data = {
                        name: iel.name,
                        value: iel.checked ? iel.value : null,
                        valueString: iel.value,
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    break;
                case "checkbox":
                    if (iel.value === "on") { // value not set in element
                        data = {
                            name: iel.name,
                            value: iel.checked,
                            valueString: iel.value,
                            validation: iel.validationMessage,
                            valid: iel.validity.valid
                        };
                    } else {
                        data = {
                            name: iel.name,
                            value: iel.checked ? String(iel.value) : null,
                            valueString: iel.value,
                            validation: iel.validationMessage,
                            valid: iel.validity.valid
                        };
                    }
                    break;
                case "file":
                    const files = iel.files as FileList;
                    // const valueFile = iel.multiple
                    //     ? files[0]
                    //     : files.length === 1 ? files[0] : null;
                    let converted: Array<any> | FileList = [];
                    switch (iel.getAttribute("convert")) {
                        case "text": {
                            for (const file of files) {
                                converted.push(await file.text());
                            }
                            break;
                        }
                        case "json": {
                            for (const file of files) {
                                converted.push(await new Response(file).json());
                            }
                            break;
                        }
                        case "base64": {
                            for (const file of files) {
                                converted.push(Buffer.from(await file.arrayBuffer()).toString("base64"));
                            }
                            break;
                        }
                        case "object": {
                            for (const file of files) {
                                converted.push({
                                    name: file.name,
                                    type: file.type,
                                    size: file.size,
                                    data: Buffer.from(await file.arrayBuffer()).toString("base64")
                            });
                            }
                            break;
                        }
                        case "dataurl": {
                            for (const file of files) {
                                const b64 = Buffer.from(await file.arrayBuffer()).toString("base64");
                                converted.push(`data:${file.type};base64,${b64}`);
                            }
                            break;
                        }
                        case "arraybuffer": {
                            for (const file of files) {
                                converted.push(await file.arrayBuffer());
                            }
                            break;
                        }
                        case "stream": {
                            for (const file of files) {
                                converted.push(file.stream());
                            }
                            break;
                        }
                        default:
                            converted = files;
                            break;
                    }
                    const value = iel.multiple
                        ? converted
                        : converted.length === 1 ? converted[0] : null;
                    const valueFile = iel.multiple
                        ? files
                        : files.length === 1 ? files[0] : null;
                    data = {
                        name: iel.name,
                        value,
                        valueFile,
                        valueString: iel.value,
                        validation: iel.validationMessage,
                        valid: iel.validity.valid
                    };
                    const maxsize = Number(iel.getAttribute("maxsize"));
                    if (maxsize) {
                        for (const file of files) {
                            if (file.size > maxsize) {
                                data.validation = `Max ${unit(maxsize)}, file ${unit(file.size)}`;
                                data.valid = false;
                            }
                        }
                    }
                    break;
            }
            break;
        case "SELECT":
            const sel = el as HTMLSelectElement;
            if (sel.multiple) {
                const values = Array.from(sel.selectedOptions).map(o => o.value);
                data = {
                    name: sel.name,
                    value: values,
                    valueString: sel.value,
                    validation: sel.validationMessage,
                    valid: sel.validity.valid
                };
            } else {
                data = {
                    name: sel.name,
                    value: sel.value === "" ? null : sel.value,
                    validation: sel.validationMessage,
                    valid: sel.validity.valid
                };
            }
            break;
        case "TEXTAREA":
            const tel = el as HTMLTextAreaElement;
            data = {
                name: tel.name,
                value: tel.value === "" ? null : tel.value,
                validation: tel.validationMessage,
                valid: tel.validity.valid
            };
            break;
        case "BUTTON":
            const bel = el as HTMLButtonElement;
            data = {
                name: bel.name,
                value: bel.value === "" ? null : bel.value,
                validation: bel.validationMessage,
                valid: bel.validity.valid
            };
            break;
    }
    return data;
}

function unit(num: number): string {
    const mb = 1e6;
    const kb = 1e3;
    if (num >= mb) {
        return `${(num / mb).toFixed(3)}${THSP}MB`;
    } else if (num >= kb) {
        return `${(num / kb).toFixed(0)}${THSP}kB`;
    } else {
        return `${num.toFixed(0)}${THSP}B`;
    }
}
