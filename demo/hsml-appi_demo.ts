import { HElements } from "../src/hsml";
import { HAction, HApp, HAppActions, HDispatch, happ } from "../src/hsml-app";

enum Actions {
    name = "name"
}

interface State {
    name: string;
}

(window as any).app = happ<State, Actions>({

    state: function (): State {
        return { name: "" };
    },

    view: function (state: State): HElements<Actions> {
        return [
            ["h1", "Hello"],
            ["input~focus", {
                type: "text",
                placeholder: "name",
                on: ["input", Actions.name]
            }],
            ["p",
                state.name
                    ? ["Hello ", ["b", state.name], "!"]
                    : "Greeting"
            ]
        ];
    },

    dispatcher: async function (this: HApp<State, Actions>, action, state, dispatch) {
        console.log(action);
        // console.log(this);
        switch (action.type) {

            case HAppActions.mount:
                this.refs["focus"].focus();
                break;

            case Actions.name:
                state.name = action.data;
                break;
        }
    },

    element: "app",
    debug: false
});
